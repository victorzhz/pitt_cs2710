/* 

   search.h 

   This file describes the interface functions you may use in search through the state space.

   If you are REALLY interested, check out search.c for implementation. This is standard
   programming handicraft and is not the matter of CS1571 course.

 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <malloc.h>

#ifndef _cs1571_search_h
#define _cs1571_search_h

const int UP_MOVE = 0, DOWN_MOVE=1, LEFT_MOVE=2, RIGHT_MOVE=3;

typedef enum {FALSE, TRUE} boolean;

const int NO_MOVE = -1;


/* *******************************************************
   STATE

   This is a representation of the state of 8-puzzle problem.
   Array indices represent the board, array values represent the moving tiles.

   ( The array indices are shifted by 1, due to C syntax, don't let that confuse you. ) 

 ****************************************************** */


typedef struct _state
{
		char a[9]; 				/* assume indices from 1-10 */	
		int empty; 				/* index of the empty cell */
		_state()			        /* initialization function  - C++ */
		{
				a[0] = '1';
				a[1] = '2';
				a[2] = '3';
				a[3] = '4';
				a[4] = '5';
				a[5] = '6';
				a[6] = '7';
				a[7] = '8';
				a[8] = '0'; 		       /* 9th cell is empty */

				empty = 8;	  	      /*index of the empty cell*/
		}
} STATE;

STATE * initialize_state(char *configuration);
void print_state(STATE *state); /* prints a state */

/* *****************************************************

   ;;   SEARCH TREE / NODE

   Nodes encapsulate states, so that general search trees may be constructed.

 ****************************************************** */

struct list_of_nodes;			    /* TECHNICALITY: forward definition of list_of_nodes structure */

typedef struct _node
{
		STATE *state;	
		struct _node *parent;
		int op;
		struct list_of_nodes *children; 
		int g_value;			    /* depth of the node in the search tree */
		int h_value;                        /* will be used for the heuristic search later */
		int f_value;                        /* will be used for the heuristic search later */
		_node()                             /* initialization function  - C++ */
		{
				parent = NULL;
				g_value = 0;
				op = -1;
				children = NULL;	
		}
} NODE;


/* initializes the search tree with init_state */
NODE *initialize_search_tree(STATE *init_state);

/* expands the node.
   - generates a new set of states from the current node's state
   - creates nodes for them and add them to the search tree
   - returns a new set of nodes in the list structure
   (if no new nodes can be generated it returns NULL) */
struct list_of_nodes *expand_node(NODE *a_node);

/* checks if the node includes the goal state */
int goalp(NODE *curr_node);


/* checks the node of the search tree for cyclic repeats */
boolean check_cyclic_repeats(NODE *curr_node);

/* extracts a list of nodes on the solution path  
   (starting from the goal node) */
struct list_of_nodes *generate_solution_list(NODE *goal_node);


/* deletes a search tree rooted at node search_tree_node */
void delete_search_tree(NODE *search_tree_node);


/* *****************************************************

   ;; LIST OF NODES functions
   ;; STUDENTS: you will need these to compute statistics 
   ;; in part b and also to implement IDA 

 ******************************************************** */

typedef struct list_of_nodes
{
		NODE *item;
		struct list_of_nodes *next;
};



/** adds an element to the front of the list ***/
struct list_of_nodes * add_to_list_front(struct list_of_nodes * old_list, 
				NODE * new_node);


/** adds an element to the rear of the list ***/
struct list_of_nodes * add_to_list_rear(struct list_of_nodes * old_list, 
				NODE * new_node);


/** accesses the first element of the list ***/
NODE * first_node_in_list(struct list_of_nodes * a_list);


/* remove the first element from the list, returns the modified remaining list */
struct list_of_nodes * remove_first_from_list(struct list_of_nodes * a_list);


/*** makes a copy of a list (its top list structure) to make it safe 
  for some operations, e.g. to concatenation **/
struct list_of_nodes * copy_list (struct list_of_nodes * a_list);


/*** deletes a list skelet of the list, items inside are not deleted **/
void delete_list (struct list_of_nodes * a_list);


/** concatenates two lists into one list (destructive operation) **/
struct list_of_nodes * concatenate_lists (struct list_of_nodes * a_list1, 
				struct list_of_nodes * a_list2);


/* computes the list length */
/* STUDENTS: you will need it to compute statistics */ 
int list_length(struct list_of_nodes * a_list);


/* prints states in the list of nodes  */
void print_all_states_in_list(struct list_of_nodes * a_list);


int returnNumNodesGen();



#endif




















