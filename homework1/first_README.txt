This is an implementation of the BFS algorithm from part a of the assignment

If you play with the program files on Unix/Linux, just type 'make' to get the
program rebuilt. 

To run the program type './bfs' 

STUDENTS: Search for 'STUDENTS' string in your editor to find hints what to 
		  look at thoroughly and what support code may be skipped.

