/* mark.h */
/* mark.c implements state marking through hash table */
/* 

   Interface to the mark structure you can use.
   You need not look into mark.c unless you really want to.

 */
#ifndef _cs1571_mark_h
#define _cs1571_mark_h

#include "search.h"

void initialize_mark_structure();
boolean check_mark(NODE *a_node);
void mark_node(NODE * a_node);
void delete_marks();

#endif
