/* main1a.c */
/* runs vanilla BFS on Puzzle 8: EXAMPLE1 and EXAMPLE2 */ 


#include "examples.c"
#include "search.h"
#include "mark.h"


/**********************************************************************
 Plain vanilla breadth first search 
***********************************************************************/

boolean bfs (char *init_configuration)
{
		STATE *init_state;
		NODE * search_tree;
		struct list_of_nodes * queue; 		/* implemented through a list of nodes, better */
		struct list_of_nodes * solution;		/* implementations exist */
		NODE * current_node;
		boolean success = FALSE;
		int numNodesGen = 0;
		int numNodesExp = 0;
		int currentLength = 0;
		int oldLength=0;

		init_state = initialize_state(init_configuration);        	/* convert the initial configuration to a state*/
		printf("\n***************************");
		printf("\n Initial configuration \n");
		print_state(init_state);
		printf("\n Hmmmmmm.....(This may take a minute)...\n");

		search_tree = initialize_search_tree(init_state);	       	/* initialize the search tree */

		/* initialize the queue */
		queue = NULL;												/* the queue is initially empty */ 
		queue=add_to_list_front(queue,search_tree);					/* we start our search with the root of search tree */
		/* do the actual search */
		int counter=0;
		while ((success == FALSE) && (queue != NULL))
		{
			
				current_node = first_node_in_list(queue); 			/* take the first node in the queue */
				//printf("The current node's state: %d", check_mark(current_node));
	
				queue = remove_first_from_list(queue);				/* and remove it from the queue */
				
				currentLength = list_length(queue);
				//printf("The current Length: %d", currentLength);
				if (goalp(current_node)) {						 	/* Did we find a solution ? */ 
						success = TRUE;

				}
				    /* YES: bail out of the cycle */
				else{
					//if(check_cyclic_repeats(current_node)==FALSE){
					//if(check_mark(current_node) == FALSE){
						mark_node(current_node);
						queue = concatenate_lists(queue,expand_node(current_node));	      /* NO: expand the node and add its children to the queue */
						numNodesExp++;
									

						currentLength = list_length(queue);
						//printf("The current Length: %d", currentLength);
						if(currentLength >= oldLength){
							currentLength = currentLength;
						}else{
							currentLength = oldLength;						
						}
					//}
				}

		} 	// end while-loop

		/* solution recovery from the search tree */ 
		if (success == TRUE) 
		{
				printf("\n Solution");
				solution = generate_solution_list(current_node);				/* generate the solution path as a list of nodes */
				print_all_states_in_list(solution);					     		/* please view .c to see how this works */
				printf("\nSolution-length: %ld \n", list_length(solution)-1);	/* output the solution */
				printf("\nThe number of nodes expanded %d\n", numNodesExp);
				numNodesGen = returnNumNodesGen();
				printf("\nThe number of nodes generated %d\n", numNodesGen);
				printf("\n The max queue length is: %d\n", currentLength+1);
				delete_marks();
		}
		else { 
				printf("\n No solution found.");
		};


		/* clean up:
		   after you are done with one search it is good to clean up so
		   that you are able to reuse the freed memory 
		 */

		printf("\n Delete solutions...");						/* delete solution list */
		delete_list(solution);
		printf("\n Delete search tree...\n");  			  		/* delete search tree structure */
		delete_search_tree(search_tree);

		return success;								   			/* tell the caller whether search was successful */
}           



/**********************************************************************
main: runs the plain bfs search on EXMAPLE1 and EXAMPLE2
 ***********************************************************************/


main()
{
		bfs(EXAMPLE_1);
		printf("\n Press return to continue ... \n"); getchar();

		bfs(EXAMPLE_2);
		printf("\n Press return to continue ... \n"); getchar();

		//Example 3 may take some time on a slow machine
		bfs(EXAMPLE_3); 

		return 0;

}





